<?php

/**
 * Implementation of hook_token_values().
 */
function payment_api_token_values($type, $object = NULL, $options = array()) {
  $values = array();
  switch ($type) {
    case 'payment':
      $methods = payment_api_methods();
      $types = payment_api_types();
      $values['payment-id'] = $object['pid'];
      $values['payment-amount'] = payment_api_format_amount($object['amount'], $object['currency']);
      $values['payment-method'] = $methods[$object['method']]['title'];
      $values['payment-type'] = $types[$object['type']]['title'];
      break;
  }
  return $values;
}

/**
 * Implementation of hook_token_list().
 */
function payment_api_token_list($type = 'all') {
  if ($type == 'payment' || $type == 'all') {
    $tokens = array();
    $tokens['payment'] = array();
    $tokens['payment']['payment-id'] = t('Payment ID');
    $tokens['payment']['payment-amount'] = t('Payment amount');
    $tokens['payment']['payment-method'] = t('Payment method');
    $tokens['payment']['payment-type'] = t('Payment type');
    return $tokens;
  }
}

/**
 * Return applicable token types for a payment.
 */
function payment_api_token_types($payment = NULL, $user = NULL) {
  $types = array(
    'payment' => $payment,
    'user' => $user,
  );
  return $types;
}

/**
 * For a given context, builds a formatted list of tokens and descriptions
 * of their replacement values.
 *
 * @param type
 *    The token types to display documentation for. Defaults to 'all'.
 * @param prefix
 *    The prefix your module will use when parsing tokens. Defaults to '['
 * @param suffix
 *    The suffix your module will use when parsing tokens. Defaults to ']'
 * @return An HTML table containing the formatting docs.
 **/
function theme_payment_api_token_help($type = 'all', $prefix = '[', $suffix = ']') {
  token_include();

  // see http://drupal.org/node/127072
  $full_list = array();
  foreach ((array)$type as $t) {
    $full_list = array_merge($full_list, token_get_list($t));
  }
  
  $headers = array(t('Token'), t('Replacement value'));
  $rows = array();
  foreach ($full_list as $key => $category) {
    $rows[] = array(array('data' => drupal_ucfirst($key) . ' ' . t('tokens'), 'class' => 'region', 'colspan' => 2));
    foreach ($category as $token => $description) {
      $row = array();
      $row[] = $prefix . $token . $suffix;
      $row[] = $description;
      $rows[] = $row;
    }
  }

  $output = theme('table', $headers, $rows, array('class' => 'description'));
  return $output;
}

/**
 * Add token help to a form or form element.
 */
function payment_api_add_token_help(&$form, $key) {
  $form[$key. '_token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form[$key. '_token_help']['help'] = array(
    '#value' => theme('payment_api_token_help', array_keys(payment_api_token_types(NULL, NULL))),
  );
}

