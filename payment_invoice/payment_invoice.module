<?php

/**
 * Implementation of hook_node_info().
 */
function payment_invoice_node_info() {
  return array(
    'payment_invoice' => array(
      'name' => t('Invoice'),
      'has_title' => TRUE,
	    'title_label' => t('Name'),
      'has_body' => TRUE,
      'body_label' => t('Details'),
      'module' => 'payment_invoice',
      'description' => t('An invoice to be submitted for later payment.'),
    )
  );
}

/**
 * Implementation of hook_access().
 */
function payment_invoice_access($op, $node) {
  global $user;

  switch ($op) {
    case 'view':
      return (user_access('view payment invoices') || user_access('view own payment invoices') && ($user->uid == $node->uid));
    case 'create':
      return user_access('create payment invoices');
    case 'update':
    case 'delete':
      return user_access('edit own payment invoices') && ($user->uid == $node->uid);
  }

}

/**
 * Implementation of hook_perm().
 */
function payment_invoice_perm() {
  return array('view payment invoices', 'view own payment invoices', 'create payment invoices', 'edit own payment invoices');
}

/**
 * Implementation of hook_form().
 */
function payment_invoice_form(&$node) {
  $type = node_get_types('type', $node);

  $form = array();
  // We need to define form elements for the node's title and body.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5
  );
  $form['body_filter'] = array(
    '#weight' => 10,
  );
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#default_value' => $node->body,
    '#required' => FALSE
  );
  $form['body_filter']['filter'] = filter_form($node->format);

  // Now we define the form elements specific to our node type.
  $form['payment_id'] = array(
    '#type' => 'value',
    '#value' => $node->payment_id,
  );

  return $form;
}

/**
 * Implementation of hook_insert().
 *
 * As a new node is being inserted into the database, we need to do our own
 * database inserts.
 */
function payment_invoice_insert($node) {
  if (empty($node->payment_id)) {
    $node->payment_id = payment_invoice_payment_api();
  }
  db_query("INSERT INTO {payment_invoice} (nid, payment_id) VALUES (%d, %d)", $node->nid, $node->payment_id);
}

/**
 * Implementation of hook_update().
 */
function payment_invoice_update($node) {
  db_query("UPDATE {payment_invoice} SET payment_id %d WHERE nid = %d", $node->payment_id, $node->nid);
}

/**
 * Implementation of hook_delete().
 */
function payment_invoice_delete($node) {
  db_query('DELETE FROM {payment_invoice} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_load().
 */
function payment_invoice_load($node) {
  $additions = db_fetch_object(db_query('SELECT payment_id FROM {payment_invoice} WHERE nid = %d', $node->nid));
  return $additions;
}

/**
 * Implementation of hook_view().
 */
function payment_invoice_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);

  if (!$teaser) {
    $payment = payment_api_load($node->payment_id);
    payment_api_view($payment);
    payment_api_preprocess($payment);
    $output = theme('payment_api_payment', $payment, variable_get('payment_api_render', PAYMENT_API_RENDER_DEFAULT));

    $node->content['payment_api_payment'] = array(
      '#value' => $output,
    );
  }
  return $node;
}

/**
 * Implementation of hook_payment_api_methods().
 */
function payment_invoice_payment_api_methods() {
  return array(
    'invoice' => array(
      'name' => 'invoice',
      'title' => t('Please Send me an Invoice'),
      'form callback' => 'payment_invoice_payment_form',
      'notification callback' => 'payment_invoice_notification',
    ),
  );
}

/**
 * Process a payment form for invoicing.
 */
function payment_invoice_payment_form(&$form, &$element) {
  global $user;

  $type = 'payment_invoice';

  if (node_access('create', $type)) {
    $node = array('uid' => $user->uid, 'name' => $user->name, 'type' => $type);
    $element[$type .'_form'] = array(
      '#type' => 'subform',
      '#id' => $type .'_node_form',
      '#arguments' => array($node),
    );
    $element['#tree'] = FALSE;
    $form['#submit']['payment_invoice_set_pid'] = array();
    $form['#submit']['subform_element_submit'] = array();
  }
}

// Not working. Replaced with hook_payment_api implementation.
function payment_invoice_set_pid($form_id, $form_values) {
  global $subforms;
  if (isset($subforms)) {
    foreach ($subforms as $key => $subform) {
      if (isset($subform['type']) && $subform['type']['#value'] == 'payment_invoice' && isset($subform['payment_id'])) {
        $subforms[$key]['payment_id']['#value'] = $form_values['pid'];
      }
    }
  }
}

function payment_invoice_payment_get_invoice($pid) {
  return db_result(db_query('SELECT nid FROM {payment_invoice} WHERE payment_id = %d', $pid));
}

function payment_invoice_notification($payment, $format) {
  static $_node_types, $_node_names;

  $node = node_load(payment_invoice_payment_get_invoice($payment['pid']));
  $node = node_build_content($node);
  $notification = array(
    'subject' =>  t('@type Invoice', array('@type' => $_node_names[$node->type])),
    'body' => theme('payment_invoice_invoice', $node, $format),
  );
  return $notification;
}

function theme_payment_invoice_invoice($node, $format) {
  return drupal_render($node->content);
}

/**
 * Implementation of hook_payment_api().
 */
function payment_invoice_payment_api($payment = NULL, $op = NULL) {
  static $pid;
  switch ($op) {
    case 'create':
      $pid = $payment['pid'];
      break;
    case NULL:
      return $pid;
  }
}
