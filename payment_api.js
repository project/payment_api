
/**
 * Attaches the payment behavior to the payment form.
 */
Drupal.paymentAttach = function() {
  $('#payment-api-form :radio[@name=method]').each(function () {
    $(this).each(function() {
      $(this).change(Drupal.paymentShowHide);
      // Trigger a change in IE.
      if ($.browser.msie) {
        $(this).click(Drupal.paymentShowHide);
      }
    });
  });
  $('.payment-required').addClass('required').prev('label').append(' <span class="form-required" title="This field is required.">*</span>');
  Drupal.paymentShowHide();
};

Drupal.paymentShowHide = function () {
  $('#payment-api-form :radio[@name=method]').each(function () {
    var checked = $(this).is(':checked');
    var value = $(this).val();
    $('fieldset.payment-fieldset-'+ value).each(function() {
      if (checked) {
        $(this).fadeIn();
      }
      else {
        $(this).hide();
      }
    });
  });
};

// Global Killswitch
if (Drupal.jsEnabled) {
  $(document).ready(Drupal.paymentAttach);
}
