<?php

/**
 * @file
 * Provides a payment API that other modules can use to declare payment
 * methods and implement payments.
 */

define('PAYMENT_API_RENDER_DEFAULT', 1);
define('PAYMENT_API_RENDER_TABLE', 2);

define('PAYMENT_API_FORMAT_SCREEN', 1);
define('PAYMENT_API_FORMAT_MAIL', 2);

/**
 * Implementation of hook_menu().
 */
function payment_api_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/payment-api',
      'title' => t('Payment API settings'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('payment_api_settings'),
      'description' => t('Set default currency and other options.'),
      'access' => user_access('administer site configuration'),
    );
  }
  else {
    payment_api_load_includes();
  }
  return $items;
}

/**
 * Menu callback for administrative settings form.
 */
function payment_api_settings() {
  $form = array();

  $form['payment_api_receipt_address'] = array(
    '#type' => 'textarea',
    '#title' => t('Receipt address'),
    '#description' => t('Enter an address to use for payment receipts.'),
    '#default_value' => variable_get('payment_api_receipt_address', ''),
  );
  
  $form['payment_api_default_currency'] = array(
    '#type' => 'select',
    '#title' => t('Default currency'),
    '#description' => t('Select a default currency for use with payments.'),
    '#default_value' => variable_get('payment_api_default_currency', 'EUR'),
    '#options' => payment_api_get_currencies(),
  );

  $form['payment_api_currencies_function'] = array(
    '#type' => 'textfield',
    '#title' => t('Currencies source'),
    '#description' => t('If desired, you may provide the name of a custom function from which to draw a list of valid currencies. Leave blank to use the default list.'),
    '#default_value' => variable_get('payment_api_currencies_function', ''),
  );

  $form['payment_api_countries_function'] = array(
    '#type' => 'textfield',
    '#title' => t('Countries source'),
    '#description' => t('If desired, you may provide the name of a custom function from which to draw a list of valid countries. Leave blank to use the default list.'),
    '#default_value' => variable_get('payment_api_countries_function', ''),
  );

  $form['payment_api_render'] = array(
    '#type' => 'radios',
    '#title' => t('Payment display format'),
    '#description' => t('Select a default display format for payments.'),
    '#default_value' => variable_get('payment_api_render', PAYMENT_API_RENDER_DEFAULT),
    '#options' => array(PAYMENT_API_RENDER_DEFAULT => t('Default: fields'), PAYMENT_API_RENDER_TABLE => t('Table')),
  );

  return system_settings_form($form);
}

/**
 * Return available payment methods.
 *
 * @param $access_filter
 *   Boolean, whether to filter the results by whether or not the current
 *   user has access to using the method.
 *
 * @return
 *   Array of methods.
 */
function payment_api_methods($access_filter = FALSE) {
  static $methods;
  if ($methods == NULL ) {
    $methods = module_invoke_all('payment_api_methods');
  }
  return $access_filter ? array_filter($methods, 'payment_api_filter_methods') : $methods;
}

/**
 * Return available payment types.
 *
 * @return
 *   Array of types.
 */
function payment_api_types() {
  static $types;
  if ($types == NULL ) {
    $types = module_invoke_all('payment_api_types');
  }
  return $types;
}

/**
 * Determine if the current user has access to a given payment method.
 *
 * @param $method
 *   Array of method data.
 *
 * @return
 *   Boolean.
 */
function payment_api_filter_methods($method) {
  return user_access('pay by '. $method['name']);
}

/**
 * Load a payment.
 *
 * @param $pid
 *   The ID of the payment to be loaded.
 *
 * @return
 *   The payment data in array form or else FALSE if no match was found.
 */
function payment_api_load($pid, $refresh = FALSE) {
  static $payments = array();
  if (!isset($payments[$pid]) || $refresh) {
    $result = db_query("SELECT * FROM {payment_api} WHERE pid = %d", $pid);
    $payment = db_fetch_array($result);
    if (!empty($payment)) {
      // Set name values for users.
      if ($account = user_load(array('uid' => $payment['by_uid']))) {
        $payment['by_name'] = $account->name;
      }
      if ($account = user_load(array('uid' => $payment['for_uid']))) {
        $payment['for_name'] = $account->name;
      }
      $payment['transaction_response'] = unserialize($payment['transaction_response']);
      payment_api_invoke_payment_api($payment, 'load');
    }
    $payments[$pid] = $payment;
  }
  return $payments[$pid];
}

/**
 * Save a payment.
 *
 * If incoming data includes a 'pid' attribute, an existing payment is
 * updated. Otherwise, a new payment is created.
 *
 * @param $payment
 *   The payment data in array form, passed by reference.
 */
function payment_api_save(&$payment) {
  if (isset($payment['transaction_response'])) {
    $payment['transaction_response'] = serialize($payment['transaction_response']);
  }
  if (isset($payment['pid'])) {
    db_query("UPDATE {payment_api} SET type = '%s', by_uid = %d, for_uid = %d, nid = %d, method = '%s', time_paid = %d, currency = '%s', amount = %d, net_amount = %d, mail = '%s', name = '%s' WHERE pid = %d, transaction_response = '%s'", $payment['type'], $payment['by_uid'], $payment['for_uid'], $payment['nid'], $payment['method'], $payment['time_paid'], $payment['currency'], $payment['amount'], $payment['net_amount'], $payment['mail'], $payment['name'], $payment['transaction_response'], $payment['pid']);
    payment_api_invoke_payment_api($payment, 'update');
  }
  else {
    $payment['pid'] = isset($payment['pre_id']) && !empty($payment['pre_id']) ? $payment['pre_id'] : db_next_id('{payment_api}_pid');
    db_query("INSERT INTO {payment_api} (pid, type, by_uid, for_uid, nid, method, time_paid, currency, amount, net_amount, mail, name, transaction_response) VALUES (%d, '%s', %d, %d, %d, '%s', %d, '%s', %d, %d, '%s', '%s', '%s')", $payment['pid'], $payment['type'], $payment['by_uid'], $payment['for_uid'], $payment['nid'], $payment['method'], $payment['time_paid'], $payment['currency'], $payment['amount'], $payment['net_amount'], $payment['mail'], $payment['name'], $payment['transaction_response']);
    payment_api_invoke_payment_api($payment, 'create');
  }
}

/**
 * Delete a payment.
 *
 * @param $pid
 *   The ID of the payment to be deleted.
 */
function payment_api_delete($pid) {
  db_query("DELETE FROM {payment_api} WHERE pid = %d", $pid);
  payment_api_invoke_payment_api($pid, 'delete');
}

/**
 * Implementation of hook_perm().
 *
 * Declare a permission for each available payment method.
 */
function payment_api_perm() {
  $perms = array('administer payments');
  foreach (payment_api_methods() as $method) {
    $perms[] = 'pay by '. $method['name'];
  }
  return $perms;
}

/**
 * Add payment information to a form.
 */
function payment_api_form($type) {
  global $user;

  $form = array();
  $methods = payment_api_methods(TRUE);
  $types = payment_api_types();
  $options = array();
  if (isset($types[$type]) && !empty($methods)) {
    $form['type'] = array(
      '#type' => 'value',
      '#value' => $type,
    );
    // Call the form function, if present.
    if (isset($types[$type]['form callback']) && function_exists($types[$type]['form callback'])) {
      $function = $types[$type]['form callback'];
      $function($form);
    }

    foreach ($methods as $method) {
      $options[$method['name']] = $method['title'];
    }
    $form['method'] = array(
      '#type' => 'radios',
      '#title' => t('Payment method'),
      '#options' => $options,
      '#default_value' => '',
      '#required' => TRUE,
    );
    foreach (array('by', 'for') as $key) {
      if (!isset($form[$key .'_uid'])) {
        $form[$key .'_uid'] = array(
          '#type' => 'value',
          '#value' => $user->uid,
        );
      }
    }
    if (!isset($form['currency'])) {
      $form['currency'] = array(
        '#type' => 'value',
        '#value' => variable_get('payment_api_default_currency', 'EUR'),
      );
    }
    if (!isset($form['transaction_response'])) {
      $form['transaction_response'] = array(
        '#type' => 'value',
        '#value' => '',
      );
    }
    if (!isset($form['pre_id'])) {
      $form['pre_id'] = array(
        '#type' => 'value',
        '#value' => '',
      );
    }
    // Register the default submit handler explicitly because more may be added below.
    $form['#submit'] = array(
      'payment_api_form_submit' => array(),
    );
    foreach ($methods as $method) {
      // Add the method's specific fields and submit/validate handlers.
      if (isset($method['form callback'])) {
        $form['payment_api_details_'. $method['name']] = array(
          '#type' => 'fieldset',
          '#title' => t('!title information', array('!title' => $method['title'])),
          '#tree' => TRUE,
          '#attributes' => array('class' => 'payment-fieldset payment-fieldset-'. $method['name']),
        );
        $method['form callback']($form, $form['payment_api_details_'. $method['name']]);
        // The fields are required only if the given method is selected.
        if (!isset($_POST['method']) || $_POST['method'] != $method['name']) {
          $form['payment_api_details_'. $method['name']]['#after_build'][] = 'payment_api_reset_required';
        }
      }
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    $path = drupal_get_path('module', 'payment_api');
    drupal_add_js($path .'/payment_api.js');
    drupal_add_css($path .'/payment_api.css');
    return $form;
  }
}

/**
 * Submit handler for payment forms.
 */
function payment_api_form_submit($form_id, &$form_values) {
  // Set uid values for users.
  if (isset($form_values['by_name']) && $account = user_load(array('name' => $form_values['by_name']))) {
    $form_values['by_uid'] = $account->uid;
  }
  if (isset($form_values['for_name']) && $account = user_load(array('name' => $form_values['for_name']))) {
    $form_values['for_uid'] = $account->uid;
  }
  // Set a net amount if none set.
  if (!isset($form_values['net_amount'])) {
    $form_values['net_amount'] = $form_values['amount'];
  }
  // Convert time from string if necessary.
  if (is_string($form_values['time_paid'])) {
    $form_values['time_paid'] = strtotime($form_values['time_paid']);
  }
  elseif (!isset($form_values['time_paid'])) {
    $form_values['time_paid'] = time();
  }
  payment_api_save($form_values);
}

/**
 * Convert required fields to a custom required class.
 *
 * This approach enables the selective requirement of fields depending on the
 * selected payment method.
 */
function payment_api_reset_required($element) {
  _payment_api_reset_required($element);
  return $element;
}

function _payment_api_reset_required(&$element) {
  foreach (element_children($element) as $key) {
    if (isset($element[$key]['#required']) && $element[$key]['#required']) {
      $element[$key]['#required'] = FALSE;
      $element[$key]['#attributes']['class'] .= ' payment-required';
    }
    // Support subforms as produced by subform_element.
    if (isset($element[$key]['#form'])) {
      _payment_api_reset_required($element[$key]['#form']);
    }
    _payment_api_reset_required($element[$key]);
  }
}

/**
 * Return an array of supported currencies.
 *
 * The list hard-coded here can be overridden with the result of a call
 * to a function registered to the variable
 * 'payment_api_currencies_function'.
 */
function payment_api_get_currencies() {
  if ($function = variable_get('payment_api_currencies_function', '')) {
    return $function();
  }
  return array(
    'EUR'  => t('Euro'),
    'USD'  => t('US Dollar'),
    'CAD'  => t('Canadian Dollar'),
  );
}

/**
 * Given an amount and a currency, return a formatted string.
 */
function payment_api_format_amount($amount, $currency) {
  $amount = number_format($amount, 2);
  switch($currency) {
    case 'EUR':
      return '€'. $amount;
    case 'USD':
    case 'CAD':
      return '$'. $amount;
    default:
      return check_plain($currency). " $amount";
  }
}

/**
 * Return an array of supported countries.
 *
 * The list hard-coded here can be overridden with the result of a call
 * to a function registered to the variable 'payment_api_countries_function'.
 */
function payment_api_get_countries() {
  if ($function = variable_get('payment_api_countries_function', '')) {
    return $function();
  }
  return array(
    'CA' => t('Canada'),
    'US' => t('United States'),
  );
}

/**
 * Invoke a hook_payment_api() operation in all modules.
 *
 * @param &$payment
 *   A payment array.
 * @param $op
 *
 * @return
 *   The returned value of the invoked hooks.
 */
function payment_api_invoke_payment_api(&$payment, $op) {
  $return = array();
  foreach (module_implements('payment_api') as $name) {
    $function = $name .'_payment_api';
    $result = $function($payment, $op);
    if (isset($result) && is_array($result)) {
      $return = array_merge($return, $result);
    }
    else if (isset($result)) {
      $return[] = $result;
    }
  }
  return $return;
}

/**
 * Load the include files used by this module.
 */
function payment_api_load_includes() {
  static $loaded = FALSE;
  if (!$loaded) {
    $path = drupal_get_path('module', 'payment_api') .'/modules';
    $files = file_scan_directory($path, '\.inc$');
    foreach ($files as $file) {
      if (module_exists($file->name)) {
        include_once $file->filename;
      }
    }
    $loaded = TRUE;
  }
}

function payment_api_view(&$payment) {
  $methods = payment_api_methods();
  $types = payment_api_types();
  $payment['content'] = array();
  $payment['content']['pid'] = array(
    '#title' => t('Payment ID'),
    '#value' => $payment['pid'],
  );
  $payment['content']['payment_time_paid'] = array(
    '#title' => t('Date'),
    '#value' => format_date($payment['time_paid'], 'custom', 'Y-m-d H:i'),
  );
  $payment['content']['payment_type'] = array(
    '#title' => t('Payment type'),
    '#value' => $types[$payment['type']]['title'],
  );
  $poster = user_load(array('uid' => $payment['by_uid']));
  $payment['content']['payment_by_uid'] = array(
    '#title' => t('Posted by'),
    '#value' => theme('username', $poster),
  );
  $payer = user_load(array('uid' => $payment['for_uid']));
  $payment['content']['payment_for_uid'] = array(
    '#title' => t('Payer'),
    '#value' => theme('username', $payer),
  );
  $payment['content']['payment_method'] = array(
    '#title' => t('Payment method'),
    '#value' => $methods[$payment['method']]['title'],
  );
  if (!empty($payment['name'])) {
    $payment['content']['payment_name'] = array(
      '#title' => t('Name of payer'),
      '#value' => check_plain($payment['name']),
    );
  }
  if (!empty($payment['mail'])) {
    $payment['content']['payment_mail'] = array(
      '#title' => t('Email of payer'),
      '#value' => check_plain($payment['mail']),
    );
  }
  if (!empty($payment['nid'])) {
    $node = node_load($payment['nid']);
    $payment['content']['payment_nid'] = array(
      '#title' => t('Content'),
      '#value' => l($node->title, 'node/'. $node->nid),
    );
  }
  $payment['content']['payment_amount'] = array(
    '#title' => t('Amount'),
    '#value' => payment_api_format_amount($payment['amount'], $payment['currency']),
    '#weight' => 10,
    '#rule_before' => TRUE,
  );
  $payment['content']['payment_net_amount'] = array(
    '#title' => t('Total'),
    '#value' => payment_api_format_amount($payment['net_amount'], $payment['currency']),
    '#weight' => 11,
    '#rule_after' => TRUE,
  );
  drupal_add_css(drupal_get_path('module', 'payment_api') .'/payment_api.css');
  payment_api_invoke_payment_api($payment, 'view');

}


/*
 * Preprocess payment content by preparing if needed and sorting by weight.
 */
function payment_api_preprocess(&$payment, $mode = PAYMENT_API_RENDER_DEFAULT) {
  if ($mode == PAYMENT_API_RENDER_DEFAULT) {
    payment_api_content_prepare($payment);
  }
  _payment_api_pre_sort($payment['content']);
  uasort($payment['content'], '_element_sort');
}

/*
 * Return themed output for a single payment.
 */
function theme_payment_api_payment($payment, $mode = PAYMENT_API_RENDER_DEFAULT, $format = PAYMENT_API_FORMAT_SCREEN) {
  switch ($mode) {
    case PAYMENT_API_RENDER_TABLE:
      foreach ($payment['content'] as $field) {
        if(isset($field['#rule_before']) && $field['#rule_before']) {
          $rows[] = array(
            array('data' => '<hr>', 'colspan' => 2),
          );
        }
        $rows[] = array(
          $field['#title'],
          $field['#value'],
        );
        if(isset($field['#rule_after']) && $field['#rule_after']) {
          $rows[] = array(
            array('data' => '<hr>', 'colspan' => 2),
          );
        }
      }
      return theme('table', array(), $rows);
    case PAYMENT_API_RENDER_DEFAULT:
    default:
      return drupal_render($payment['content']);
  }
}

function payment_api_content_prepare(&$payment) {
  $content = array();
  foreach (element_children($payment['content']) as $key) {
    if (isset($payment['content'][$key]['#rule_before']) && $payment['content'][$key]['#rule_before']) {
      $content['rule_'. $key .'_before'] = _payment_api_rule(isset($payment['content'][$key]['#weight']) ? $payment['content'][$key]['#weight'] - 1 : 0);
    }
    $content[$key] = array(
      '#weight' => isset($payment['content'][$key]['#weight']) ? $payment['content'][$key]['#weight'] : 0,
      '#value' => theme('payment_api_field', $key, $payment['content'][$key]['#title'], $payment['content'][$key]['#value']),
    );
    if (isset($payment['content'][$key]['#rule_after']) && $payment['content'][$key]['#rule_after']) {
      $content['rule_'. $key .'_after'] = _payment_api_rule(isset($payment['content'][$key]['#weight']) ? $payment['content'][$key]['#weight'] + 1 : 0);
    }
  }
  $payment['content'] = $content;
}

function _payment_api_pre_sort(&$element) {
  // Sort the elements by weight.
  // Because uasort doesn't keep the original order for equal values,
  // we assign a nominal weight to all elements.
  foreach (element_children($element) as $count => $key) {
    if (!isset($element[$key]['#weight']) || $element[$key]['#weight'] == 0) {
      $element[$key]['#weight'] = $count/1000;
    }
  }
}

function _payment_api_rule($weight = 0) {
  return array(
    '#value' => '<hr />',
    '#weight' => $weight,
  );
}

function theme_payment_api_field($name, $title, $value) {
  $output = '<div class="payment-api-field payment-api-field-'. $name .'">';
  $output .= '<div class="payment-api-field-label field-label">'. $title .'</div>';
  $output .= '<div class="payment-api-field-item field-item">'. $value .'</div>';
  $output .= '</div>';
  return $output;
}
